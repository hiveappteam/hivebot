#!/bin/bash
if [[ $(env | grep VIRTUAL_ENV | wc -l) -eq 0 ]]; then
    source venv/bin/activate
fi
python -m hivebot

FROM python:3.7-slim

WORKDIR app

COPY requirements.txt ./
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt

COPY hivebot hivebot
COPY run.sh run.sh

CMD ["./run.sh"]

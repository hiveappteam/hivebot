import requests
import logging
import os

from telegram import (
    ParseMode, InlineKeyboardMarkup, InlineKeyboardButton, Update,
    KeyboardButton, ReplyKeyboardMarkup,
    ReplyKeyboardRemove
)
from telegram.ext import (
    Updater,
    CommandHandler,
    ConversationHandler,
    CallbackQueryHandler,
    Filters,
    CallbackContext,
    MessageHandler
)

# Enable logging
from telegram.utils import helpers

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)

logger = logging.getLogger(__name__)

OPTION, ADD, TOP, DONE = map(chr, range(1, 5))
HOSTNAME = os.environ.get('HOSTNAME', None)
END = ConversationHandler.END

def start_inline(update: Update, context: CallbackContext) -> None:
    buttons = [
        [
            InlineKeyboardButton(text='⛰ TOP', callback_data=str(TOP)),
            InlineKeyboardButton(text='💩 ADD', callback_data=str(ADD)),
        ]
    ]
    keyboard = InlineKeyboardMarkup(buttons)
    text = '🚀 you may request current top of hive tags or add your tag 🚀'
    update.message.reply_text(text=text, reply_markup=keyboard)


def start(update: Update, context: CallbackContext) -> int:
    logger.info("%s -> %s", update.effective_user, update.message.text)

    reply_keyboard = [['⛰ TOP', '💩 ADD'], ['Other']]

    update.message.reply_text(
        '🚀 You may request current top of hive tags or add your tag 🚀',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False),
    )

    return OPTION


def create_buttons(record):
    return [
        InlineKeyboardButton(text=f"🏁 {record['name']}", callback_data=str(record['id'])),
        InlineKeyboardButton(text=f"↑ {record['upvotes']}", callback_data=f"{str(record['id'])}:up"),
        InlineKeyboardButton(text=f"↓ {record['downvotes']}", callback_data=f"{str(record['id'])}:down"),
    ]


def option(update: Update, context: CallbackContext) -> int:
    logger.info("%s -> %s", update.effective_user, update.message.text)

    text = None
    if 'TOP' in update.message.text:
        return top(update, context)
    if 'Other' in update.message.text:
        text = 'Option is to be implemented.. /cancel or /start over'
    if 'ADD' in update.message.text:
        text = 'Option is to be implemented.. /cancel or /start over'

    update.message.reply_text(text, reply_markup=ReplyKeyboardRemove())
    return DONE


def process_voting(update: Update, context: CallbackContext) -> int:
    logger.info("%s -> %s", update.effective_user, update.callback_query.data)

    lst = update.callback_query.data.split(':')
    if HOSTNAME is not None:
        r = requests.put(HOSTNAME + f"/api/v1/tags/{lst[1]}/{lst[0]}")
        logging.info(r.status_code)

    if HOSTNAME is not None:
        r = requests.get(HOSTNAME + '/api/v1/tags')
        if r.status_code == 200:
            buttons = [create_buttons(x) for x in r.json()]
            update.callback_query.answer()
            update.callback_query.edit_message_text('🚀 Behold THE TOP 🚀', reply_markup=InlineKeyboardMarkup(buttons))
            return OPTION
    return OPTION 


def help(update: Update, context: CallbackContext) -> None:
    logger.info("%s -> %s", update.effective_user, update.message.text)

    bot = context.bot
    text = "/start"
    update.message.reply_text(text)


def top(update: Update, context: CallbackContext) -> int:
    logger.info("%s -> %s", update.effective_user, update.message.text)

    if HOSTNAME is not None:
        r = requests.get(HOSTNAME + '/api/v1/tags')
        if r.status_code == 200:
            buttons = [create_buttons(x) for x in r.json()]
            update.message.reply_text('🚀 Behold THE TOP 🚀', reply_markup=InlineKeyboardMarkup(buttons))
            return OPTION
        else:
            text = 'Request failed. /cancel or /start over'
    else:
        text = 'API hostname is not set yet..'

    update.message.reply_text(text, reply_markup=ReplyKeyboardRemove())
    return DONE


def cancel(update: Update, context: CallbackContext) -> int:
    logger.info("%s -> %s", update.effective_user, update.message.text)
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        f"Hi, {update.message.from_user.username}! your options: /start, /cancel", reply_markup=ReplyKeyboardRemove()
    )

    return DONE


def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(os.environ.get('TOKEN', None))

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler('start', start),
            CommandHandler('top', top),
            CallbackQueryHandler(process_voting, pattern='^.*(:up|:down)$')
        ],
        states={
            OPTION: [MessageHandler(Filters.regex('^(⛰ TOP|💩 ADD|Other)$'), option)],
            DONE: [MessageHandler(Filters.all, cancel)],
        },
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(conv_handler)

    webhook = updater.start_webhook(listen='0.0.0.0', port=5001, url_path='TOKEN1', webhook_url='https://hivebot.info/TOKEN1')
    updater.bot.set_webhook(url='https://hivebot.info/TOKEN1')
    updater.idle()


if __name__ == "__main__":
    main()
